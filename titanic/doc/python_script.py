import numpy 
import theano
import theano.tensor as T
import csv as csv 

#Открываем и читаем csv файл
csv_file_object = csv.reader(open('./titanik_train_final.csv', 'rb'), delimiter='\t')
data=[]                                     #Создаем переменную-массив
for row in csv_file_object:      # Каждую строку
    data.append(row)                # заносим в  массив data
data = numpy.array(data)        # Превращаем обычный массив в тип numpy.array 
data = data.astype('float')        # Конвертируем в тип float
Y = data[:,1]                            # Все строки первого столбца заносим в Y
X = data[:,2:]                           # Все строки со второго столбца и до последнего заносим в X 
#Аналогично для тестовой выборки, только без Y
csv_file_object = csv.reader(open('./titanik_test_final.csv', 'rb'), delimiter='\t')
data_test=[]                         
for row in csv_file_object:     
    data_test.append(row)           
Tx = numpy.array(data_test)
Tx = Tx.astype('float')
Tx = Tx[:,1:]
#Создаем объект для получения случайных чисел
rng = numpy.random
#Количество строк
N = 891
#Количество столбцов
feats = 56
#Количество тренировочных шагов
training_steps = 10000

# Декларируем символьные переменные Theano 
x = T.matrix("x")
y = T.vector("y")
w = theano.shared(rng.randn(feats), name="w")
b = theano.shared(0., name="b")

# Создаем «выражение» Theano
p_1 = 1 / (1 + T.exp(-T.dot(x, w) - b))           # Вероятность того, что результат равен  1
prediction = p_1 > 0.5                                    # Порог для прогнозирования
xent = -y * T.log(p_1) - (1-y) * T.log(1-p_1) # Функция ошибки для перекрестной энтропии
cost = xent.mean() + 0.01 * (w ** 2).sum()  # Стоимость минимизации
gw,gb = T.grad(cost, [w, b])                          # Рассчитываем градиент стоимости
                                          
# Компилируем «выражение» Theano
train = theano.function(
          inputs=[x,y],
          outputs=[prediction, xent],
          updates=((w, w - 0.1 * gw), (b, b - 0.1 * gb)))
predict = theano.function(inputs=[x], outputs=prediction)

# Тренировка модели
for i in range(training_steps):
    pred, err = train(X, Y)

#Прогнозирование
P = predict(Tx)
#Сохраняем в файл
numpy.savetxt('./autoencoder.csv',P,'%i')