var INDEX_DATE = 0;
var INDEX_GENERATED = 1;
var INDEX_CONSUMED = 2;


function calculateZn1(input)
{
    var ret = [];

    for (var i=1; i<input.length; ++i)
    {
        ret.push( input[i][INDEX_CONSUMED] - input[i-1][INDEX_CONSUMED] );
    }

    return ret;
}


function estimateM(zn1)
{
    var total = zn1.length; 

    var basePeriod = [0, Math.floor(total * 0.4)];   
    var testPeriod = [basePeriod[1], basePeriod[1]*2]; 
    var controlPeriod = [testPeriod[1], total];

    // TODO    
}


// Z
function findSimilarity(zn1, limitZn1, M, Q, debug)
{
    var sigmaSqMin, jMin, sigmaSq;

    for (var j=0; j<limitZn1 - M - Q.length; ++j)
    {
        sigmaSq = 0;

        for (var i=0; i<M; ++i)
        {
            sigmaSq += Math.pow(zn1[i+j] - Q[i], 2);
        }

        if (!sigmaSqMin || sigmaSqMin > sigmaSq)
        {
            sigmaSqMin = sigmaSq; 
            jMin = j;
        }
    }

    debug.text.push("Ряд Z начинается с индекса "+jMin );
    debug.text.push("Квадратичное отклонение: "+sigmaSqMin );

    return jMin;
}


function solvePredictionEquations(zn1, M, Z, zStart, Q, P, debug)
{
    var negZ = [], posZ = [], negQ = [], posQ = [];

    for (var i=0; i<Z.length; ++i)
    {
        if (Z[i] < 0)
            negZ.push(Z[i]);
        else 
            posZ.push(Z[i]);
    }
    for (var i=0; i<Q.length; ++i)
    {
        if (Q[i] < 0)
            negQ.push(Q[i]);
        else 
            posQ.push(Q[i]);
    }

    debug.text.push(Z.toString());
    debug.text.push("Положительные значения Z:"+posZ.toString());
    debug.text.push("Отрицательные значения Z:"+negZ.toString());
    debug.text.push("Положительные значения Q:"+posQ.toString());
    debug.text.push("Отрицательные значения Q:"+negQ.toString());

    var padNeg = Math.max(negZ.length, negQ.length);
    for (var i=negZ.length; i<padNeg; ++i)
        negZ.push(0);
    for (var i=negQ.length; i<padNeg; ++i)
        negQ.push(0);

    var padPos = Math.max(posZ.length, posQ.length);
    for (var i=posZ.length; i<padPos; ++i)
        posZ.push(0);
    for (var i=posQ.length; i<padPos; ++i)
        posQ.push(0);


    function sumFuncSq(previousValue, currentValue) {
        return previousValue + Math.pow(currentValue,2);
    }

    function sumFunc(previousValue, currentValue) {
        return previousValue + currentValue;
    }

    function mulFunc(someArray, previousValue, currentValue, i) {
        return previousValue + currentValue * someArray[i];
    }

    var negZSqSum = negZ.reduce(sumFuncSq,0);
    var posZSqSum = posZ.reduce(sumFuncSq,0);
    var negZSum = negZ.reduce(sumFunc, 0);
    var posZSum = posZ.reduce(sumFunc, 0);
    var negQSum = negQ.reduce(sumFunc, 0);
    var posQSum = posQ.reduce(sumFunc, 0);
    var negQZSum = negZ.reduce( mulFunc.bind(null,negQ), 0 );
    var posQZSum = posZ.reduce( mulFunc.bind(null,posQ), 0 );

    // ZSqSum * alpha1 + ZSum * alpha0 = QZ 
    // ZSum * alpha1 + M * alpha0 = QSum    ==>  alpha0 = (QSum - ZSum * alpha1) / M

    // ZSqSum * alpha1 - ZSum^2 * alpha1 / M + ZSum * QSum / M = QZ
    // alpha1 * ( ZSqSum - ZSum^2 / M )  + ZSum * QSum / M  = QZ
    // alpha1 = (QZ - ZSum * QSum / M) / ( ZSqSum - ZSum^2 / M )

    var alpha1Pos = (posQZSum - posZSum * posQSum / M) / ( posZSqSum - Math.pow(posZSum,2) / M );
    var alpha0Pos = (posQSum - posZSum * alpha1Pos) / M;

    var alpha1Neg = (negQZSum - negZSum * negQSum / M) / ( negZSqSum - Math.pow(negZSum,2) / M );
    var alpha0Neg = (negQSum - negZSum * alpha1Neg) / M;

    debug.text.push("Найдены коэффициенты:");
    debug.text.push("\tДля положительных отклонений: a0="+alpha0Pos+", alpha1="+alpha1Pos);
    debug.text.push("\tДля отрицательных отклонений: a0="+alpha0Neg+", alpha1="+alpha1Neg);

    var prediction = [];

    for (var i=0; i<P; ++i)
    {
        var q = zStart+Z.length + i;
        if (zn1[q] < 0)
            prediction.push( alpha1Neg * zn1[q] + alpha0Neg );
        else
            prediction.push( alpha1Pos * zn1[q] + alpha0Pos );
    }

    debug.text.push("Прогнозируемые значения отклонений: " + prediction.toString());

    return prediction;
}