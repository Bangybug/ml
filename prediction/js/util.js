function formatDate(d) 
{
    var
        month = String(d.getMonth() + 1),
        day = String(d.getDate()),
        year = String(d.getFullYear());

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [day, month, year].join('.');
}


function dateIncrement(d, day, month)
{
    if (day)
        d.setDate(d.getDate() + 1);
    else 
        d.setMonth(d.getMonth() + 1);
}


function saveFile(toSave, saveAs)
{
    if (window.navigator.msSaveBlob)
    {
        var blob1 = new Blob(
            [toSave], 
            { type: "text/plain", charset: "utf-8" } 
        );
        window.navigator.msSaveBlob(blob1, saveAs); 
    }
    else
    {
        var pom = document.createElement('a');
        pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(toSave));
        pom.setAttribute('download', saveAs);
        pom.setAttribute('target','_blank');
        if (document.createEvent) 
        {
            var event = document.createEvent('MouseEvents');
            event.initEvent('click', true, true);
            pom.dispatchEvent(event);
        }
        else 
        {
            pom.click();
        }
    }

}




function parseFromTo(dateFrom, count, days)
{
    var count;
    if (!days)
    {
        var url = "http://so-ups.ru/index.php?id=973&tx_ms1cdu_pi1%5Bkpo%5D=1019&tx_ms1cdu_pi1%5Bdt%5D=";
    }
    else
    {
        var url = "http://so-ups.ru/index.php?id=972&tx_ms1cdu_pi1%5Bkpo%5D=1019&tx_ms1cdu_pi1%5Bdt%5D=";
    }

    var tomorrow = dateFrom;
    dateFrom = new Date(dateFrom.getTime());

    var docStub = jQuery('#doc-stub');
    if (!docStub.length)
    {
        jQuery('<div id="doc-stub"></div>').appendTo('body');
        docStub = jQuery('#doc-stub');
    }

    var parsedContent = [[
        "Date", "Generated", "Consumed"
    ]];

    for (var i=0; i<count; ++i)
    {
        jQuery.ajax({
          url: url + formatDate(tomorrow),
          async: false
        }).done(function(msg) {
            
            var bodyText = msg.substring(msg.indexOf("<body>")+6, msg.indexOf("</body>"));
            docStub.empty();
            docStub.append( jQuery.parseHTML( bodyText ) );

            var rows = $('#doc-stub .graph_data tr');
            for (var j=1; j<rows.length; ++j)
            {
                var row = $(rows.get(j));
                var cols = row.find('td');

                var rowData = [];

                for (var k=0; k<cols.length; ++k)
                {
                    var col = $(cols[k]);
                    var colValue = col.text();
                    rowData.push( colValue );
                }

                parsedContent.push(rowData);
            }

        });

        if (days)
            dateIncrement(tomorrow, 1, 0);           
        else
            dateIncrement(tomorrow, 0, 1);             
    }


    if (typeof Papa !== 'undefined')
    {
        var csv = Papa.unparse(parsedContent, {
            dynamicTyping: true
        });
    }
    else
    {
        // var csv = JSON.stringify(parsedContent);
        var csv = parsedContent.map(
            function(e) {
                return e.join(';');
            }).join('\n');

        saveFile( csv, "data_"+(days ? (count +"_days") : (count + "_months"))+"_from_"+formatDate(dateFrom)+".csv" );
    }


    console.log(csv);
}

