var chartSvg = null;
var xAxis, yAxis, valueline, width, height,x,y;

$(document).ready( function()
{
    input = input.slice(1,input.length);
    loadInput(input);
    initChart();
    $('#load-project input').get(0).addEventListener('change', loadFromFile );

    $('#open').click( function(e) {

    });

    $('#predict').click( function(e){
        var limit = $('#set-limit').is(':checked') ? parseInt($('#limit').val()) : input.length;
        limit -=1;

        var debug = {text:[]};

        var P = parseInt( $('#P').val() );
        var M = parseInt( $('#M').val() );
        var zn1 = calculateZn1( input );
        var Q = zn1.slice( limit-M, limit );
        var zStart = findSimilarity(zn1, limit, M, Q, debug);
        var Z = zn1.slice( zStart, zStart+M );      
        var prediction = solvePredictionEquations(zn1, M, Z, zStart, Q, P, debug);

        if (limit < zn1.length)
        {
            var beyondLimit = [];
            for (var i=limit; i<Math.min(limit+P, zn1.length); ++i)
                beyondLimit.push(zn1[i]);
            debug.text.push("Известные отклонения (для проверки прогноза): "+beyondLimit.toString());
        }

        var debugText = debug.text.join('\n');
        console.log(debugText);
        $('#debug').text(debugText);


        var chartData = zn1.map( function(e,i) {
            return [input[i][0], e];
        });
        setupChart(chartData);

    });
});





function loadInput(input)
{
    $('#load-count').text( input.length );
    var minD, maxD;

    for (var i=0; i<input.length; ++i)
    {
        var dateTime = input[i][0].split(' ');
        var dmy = dateTime[0].split('-');
        var d = input[i][0] = new Date( [dmy[2],dmy[1],dmy[0]].join('-') );

        if (dateTime.length > 1)
        {
            var hm = dateTime[1].split(':');
            if (hm.length > 0)
                d.setHours(parseInt(hm[0]));
        }

        if (!minD || minD > d)
            minD = d;
        if (!maxD || maxD < d)
            maxD = d;
    }

    $('#date-from').text( formatDate(minD) );
    $('#date-to').text( formatDate(maxD) );
    $('#limit').val( input.length );

    window.input = input;
}


function initChart()
{
    var margin = {top: 20, right: 20, bottom: 30, left: 90};
    width = $(window).width() - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;


    // Set the ranges
    x = d3.time.scale().range([0, width]);
    y = d3.scale.linear().range([height, 0]);

    // Define the axes
    xAxis = d3.svg.axis().scale(x)
        .orient("bottom").ticks(5);

    yAxis = d3.svg.axis().scale(y)
        .orient("left").ticks(5);


    // Define the line
    valueline = d3.svg.line()
        .x(function(d) { return x(d[0]); })
        .y(function(d) { return y(d[1]); });

    // Adds the svg canvas
    chartSvg = d3.select("#plot")
        .append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
        .append("g")
            .attr("transform", 
                  "translate(" + margin.left + "," + margin.top + ")");

    // Add the X Axis
    chartSvg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

    // Add the Y Axis
    chartSvg.append("g")
        .attr("class", "y axis")
        .call(yAxis);

}


function loadFromFile(e)
{
    if(!e.target.files.length) return;
    var file = e.target.files[0];

    var reader = new FileReader();          
    function receivedText() {
        var content = reader.result;
        try
        {
            var csvRows = Papa.parse(content, {
                dynamicTyping: true
            }).data;

            csvRows = csvRows.slice(1,csvRows.length);

            loadInput(csvRows);
        }
        catch(e)
        {
            console.error(e);
            alert('Ошибка загрузки файла');
        }
    }
    
    reader.onload = receivedText;
    reader.readAsText(file);
};


function setupChart(data)
{
    // Scale the range of the data
    x.domain(d3.extent(data, function(d) { return d[0]; }));
    y.domain([d3.min(data, function(d) { return d[1]; }), d3.max(data, function(d) { return d[1]; })]);

    // Add the valueline path.
    var lines = chartSvg.selectAll(".line")   // change the line
        .data(data);

    lines
        .enter().append('path').attr('class','line');
    lines
        .exit().remove();
    
    lines
        .attr("d", function(d,i) { return valueline([ data[i], data.length === i+1 ? data[i] : data[i+1] ])  } );

    xAxis = xAxis.scale(x)
        .orient("bottom").ticks(5);

    yAxis = yAxis.scale(y)
        .orient("left").ticks(5);

    chartSvg.select(".x.axis") // change the x axis
        .call(xAxis);
    chartSvg.select(".y.axis") // change the y axis
        .call(yAxis);        


}