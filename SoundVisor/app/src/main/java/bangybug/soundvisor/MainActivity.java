package bangybug.soundvisor;

import bangybug.soundvisor.audio.AudioDataSaver;
import bangybug.soundvisor.audio.RecordingThread;
import bangybug.soundvisor.detector.NoDetector;
import bangybug.soundvisor.webserver.SimpleWebServer;
import bangybug.soundvisor.audio.Constants;


import android.Manifest;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Build;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    private final String[] permissions = {
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.INTERNET,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };
    private final int MY_PERMISSIONS_REQUEST = 999;


    private SimpleWebServer server;
    private RecordingThread recorder;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkPermissions();
    }

    private void initWithPermissions()
    {
        File tmpDir = this.getDir("tmp", Context.MODE_PRIVATE);
        Constants.DEFAULT_WORK_SPACE = tmpDir.getAbsolutePath() + "/";
        Constants.SAVE_AUDIO = Constants.DEFAULT_WORK_SPACE + "recording.pcm";

        final int port = 8081;
        server = new SimpleWebServer(port, getResources().getAssets());
        server.start();

        recorder = new RecordingThread(messageHandler, new AudioDataSaver(), new NoDetector());
    }


    private void checkPermissions()
    {
        try {
            boolean allGranted = true;
            for (int i=0; i<permissions.length; ++i) {
                if (ContextCompat.checkSelfPermission(getApplicationContext(), permissions[i]) != PackageManager.PERMISSION_GRANTED) {
                    allGranted = false;
                    break;
                }
            }

            if (!allGranted)
            {
                if (Build.VERSION.SDK_INT >= 23)
                {
                    requestPermissions(permissions,  MY_PERMISSIONS_REQUEST);
                }
            }
            else
                initWithPermissions();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    public Handler messageHandler = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            MsgEnum message = MsgEnum.getMsgEnum(msg.what);
            switch (message)
            {
                case MSG_HOTWORD_DETECTED:
                    //beeper.play();
                    //visualizer.setColor(SPECTRUM_COLOR_RECORDING);
                    break;
                case MSG_BEEP_PLAYED:
                    recorder.startRecording( 3000 );
                    break;
                case MSG_RECORD_STOP:
                    //visualizer.setColor(SPECTRUM_COLOR_IDLE);
                    //transcriber.transcribeAsync( new File(Constants.SAVE_AUDIO));
                    break;
            }
        }
    };



}