package bangybug.soundvisor.audio;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import bangybug.soundvisor.MsgEnum;
import bangybug.soundvisor.detector.IDetector;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Created by drpad on 31.10.2017.
 */

public class RecordingThread extends Thread
{
    private static String TAG = RecordingThread.class.getName();

    private Object sleeper = new Object();
    private boolean shutdown = false;
    private CountDownLatch shutdownLatch;

    private boolean isListening, isRecording;

    private AudioDataSaver audioSaver;
    private AudioDataReceivedListener audioReceiveListener;

    private Handler messageHandler;

    private long recordingTime;
    private IDetector detector;

    public RecordingThread(Handler messageHandler, AudioDataReceivedListener audioReceiveListener, IDetector detector)
    {
        this.messageHandler = messageHandler;
        shutdownLatch = new CountDownLatch(1);
        audioSaver = new AudioDataSaver();
        this.audioReceiveListener = audioReceiveListener;
        this.detector = detector;
    }


    public void startListening()
    {
        synchronized (sleeper)
        {
            isListening = true;
            isRecording = false;
            detector.reset();
            sleeper.notify();
        }
    }


    public void stopListening()
    {
        synchronized (sleeper)
        {
            isListening = false;
            sleeper.notify();
        }
    }


    public void startRecording(int recordingTime)
    {
        synchronized (sleeper)
        {
            this.recordingTime = System.currentTimeMillis() + recordingTime;
            isRecording = true;
            isListening = false;
            sleeper.notify();
            audioSaver.start();
        }
    }




    public void shutdown()
    {
        synchronized (sleeper)
        {
            shutdown = true;
            sleeper.notify();
            try
            {
                shutdownLatch.await(3000, TimeUnit.MILLISECONDS);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
    }


    private void sendMessage(MsgEnum what, Object obj)
    {
        if (null != messageHandler)
        {
            Message msg = messageHandler.obtainMessage(what.ordinal(), obj);
            messageHandler.sendMessage(msg);
        }
    }




    public void run()
    {
        AudioRecord record = null;

        try
        {
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_AUDIO);

            // Buffer size in bytes: for 0.1 second of audio
            int bufferSize = (int) (Constants.SAMPLE_RATE * 0.1 * 2);

            byte[] audioBuffer = new byte[bufferSize];
            record = new AudioRecord(
                    MediaRecorder.AudioSource.DEFAULT,
                    Constants.SAMPLE_RATE,
                    AudioFormat.CHANNEL_IN_MONO,
                    AudioFormat.ENCODING_PCM_16BIT,
                    bufferSize);

            if (record.getState() != AudioRecord.STATE_INITIALIZED) {
                Log.e(TAG, "Audio Record can't initialize!");
                return;
            }

            record.startRecording();


            while (!shutdown)
            {
                synchronized (sleeper)
                {
                    try
                    {
                        if (isListening || isRecording)
                        {
                            int readCount = record.read(audioBuffer, 0, audioBuffer.length);
                            short[] audioData = new short[audioBuffer.length / 2];
                            ByteBuffer.wrap(audioBuffer).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(audioData);

                            // usually it is something wrong
                            if (0 == readCount)
                                continue;

                            if (isListening)
                            {
                                int detection = detector.detect(audioData, audioData.length);
                                if (detection == IDetector.RESULT_DETECTED)
                                {
                                    sendMessage(MsgEnum.MSG_HOTWORD_DETECTED, null);
                                    isListening = false;
                                }
                            }
                            if (isRecording)
                            {
                                audioSaver.onAudioDataReceived(audioBuffer, audioBuffer.length);
                                if (recordingTime < System.currentTimeMillis())
                                {
                                    isRecording = false;
                                    sendMessage(MsgEnum.MSG_RECORD_STOP, null);
                                }
                            }

                            audioReceiveListener.onAudioDataReceived(audioBuffer, audioBuffer.length);
                        }
                        else
                        {
                            audioReceiveListener.stop();
                            sleeper.wait();
                        }
                    }
                    catch (InterruptedException e)
                    {
                        Log.e(TAG, e.getMessage());
                        break;
                    }
                }
            }
        }
        finally
        {
            audioSaver.stop();

            try
            {
                if (record != null)
                {
                    record.stop();
                    record.release();
                }
            }
            catch (Throwable e)
            {
                Log.e(TAG, e.getMessage());
            }

            shutdownLatch.countDown();
        }
    }
}
