package bangybug.soundvisor.audio;

/**
 * Created by drpad on 31.10.2017.
 */

public class Constants
{
    public static final int SAMPLE_RATE = 16000;
    public static String SAVE_AUDIO = "recording.pcm";
    public static String DEFAULT_WORK_SPACE = "";
}
