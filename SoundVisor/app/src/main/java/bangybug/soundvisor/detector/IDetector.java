package bangybug.soundvisor.detector;

/**
 * Created by dpryadkin on 31.10.2017.
 */

public interface IDetector
{
    public static final int RESULT_UNKNOWN_ERROR = -1;
    public static final int RESULT_NO_SPEECH = -2;
    public static final int RESULT_NOT_DETECTED = 0;
    public static final int RESULT_DETECTED = 1;


    void reset();
    int detect(short[] audioData, int len);
    void setSensitivity(float sens);
    void setGain(float gain);
}
