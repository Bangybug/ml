package bangybug.soundvisor.detector;

/**
 * Created by dpryadkin on 31.10.2017.
 */

public class NoDetector implements IDetector
{
    public void reset()
    {

    }

    public void setSensitivity(float sens)
    {}

    public int detect(short[] audioData, int len)
    {
        return RESULT_NOT_DETECTED;
    }

    public void setGain(float gain)
    {}
}
