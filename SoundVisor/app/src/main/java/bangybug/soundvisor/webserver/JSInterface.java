package bangybug.soundvisor.webserver;
import android.webkit.JavascriptInterface;

/**
 * Created by bugdog on 24.10.17.
 */

public class JSInterface {

    private static final String TAG = JSInterface.class.getSimpleName();
    private String input;



    private  String quote(String string) {
        if (string == null || string.length() == 0) {
            return "\"\"";
        }

        char         c = 0;
        int          i;
        int          len = string.length();
        StringBuilder sb = new StringBuilder(len + 4);
        String       t;

        sb.append('"');
        for (i = 0; i < len; i += 1) {
            c = string.charAt(i);
            switch (c) {
                case '\\':
                case '"':
                    sb.append('\\');
                    sb.append(c);
                    break;
                case '/':
                    //                if (b == '<') {
                    sb.append('\\');
                    //                }
                    sb.append(c);
                    break;
                case '\b':
                    sb.append("\\b");
                    break;
                case '\t':
                    sb.append("\\t");
                    break;
                case '\n':
                    sb.append("\\n");
                    break;
                case '\f':
                    sb.append("\\f");
                    break;
                case '\r':
                    sb.append("\\r");
                    break;
                default:
                    if (c < ' ') {
                        t = "000" + Integer.toHexString(c);
                        sb.append("\\u" + t.substring(t.length() - 4));
                    } else {
                        sb.append(c);
                    }
            }
        }
        sb.append('"');
        return sb.toString();
    }




    @JavascriptInterface
    public synchronized String getInput()
    {
        String ret =  "{}";
        input = "";
        return ret;
    }

    public synchronized  byte[] setInput(String queryString)
    {
        input = queryString;
        return new byte[0];
    }

    public synchronized byte[] setInputAsString(String plainString)
    {
        return setInput( "{\"plain\":"+quote(plainString)+"}" );
    }



}
