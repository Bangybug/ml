package bangybug.soundvisor;

public enum  MsgEnum {
    MSG_BEEP_PLAYED,
    MSG_RECORD_START,
    MSG_RECORD_STOP,
    MSG_HOTWORD_DETECTED;

    public static MsgEnum getMsgEnum(int i) {
        return MsgEnum.values()[i];
    }
}
